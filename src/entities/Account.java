package entities;

public class Account { // acc

    private final int id;
    private String username;
    private String password;

    public Account(int id, String username, String password) {
        this.id = id;
        this.setName(username);
        this.setPassword(password);
    }

    public int getId() { return id; }

    public String getName() {
        return username;
    }

    public void setName(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}
