package entities;

public class Car {

    private final int id;
    private final User renter; // int type???

    private String model;
    private double price;
    private Boolean available;

    public Car(int id, User renter, String model, double price) {
        this.id = id;
        this.renter = renter;
        this.setModel(model);
        this.setPrice(price);
        this.setAvailable(true);
    }

    public void view() {
        System.out.println("Renter: " + renter.getUsername());
        System.out.println("Model: " + model);
        System.out.println("price: " + price);
    }

    public int getId() {
        return id;
    }

    public User getRenter() {
        return renter;
    }

    public String getModel() {
        return model;
    }

    public void setModel(String model) {
        this.model = model;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public Boolean isAvailable() {
        return available;
    }

    public void setAvailable(Boolean state) {
        this.available = state;
    }
}
