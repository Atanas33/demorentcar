package entities;

import java.util.List;
import java.util.Map;
import java.util.Scanner;

public class UI {

    public static void userActions(RentCarSite site) {
        Scanner input = new Scanner(System.in);

        while (true) {
            int choice = -1;
            System.out.println("1. Login");
            System.out.println("2. Registration");
            System.out.println("3. View all available cars");
            System.out.println("0. exit" + '\n');

            while (choice < 0 || choice > 3) {
                System.out.print("Choice: ");
                choice = input.nextInt();
            }

            switch (choice) {
                case 1:
                    login(site);
                    break;
                case 2:
                    registration(site);
                    break;
                case 3:
                    viewAvailableCars(null, site);
                    break;
                case 0:
                    return;
            }
            System.out.println();
        }
    }

    public static void login(RentCarSite site) {
        Scanner input = new Scanner(System.in);
        String username, password;

        System.out.print('\n' + "Username: ");
        username = input.nextLine();

        System.out.print("Password: ");
        password = input.nextLine();

        User user = site.login(username, password);

        if (user != null) {
            while (true) {
                int choice = -1;

                System.out.println('\n' + "1. View all available cars");
                System.out.println("2. View your rented cars");
                System.out.println("3. View your cars for rent");
                System.out.println("4. Add car for rent");
                System.out.println("0. Sign out");

                System.out.println();
                while (choice < 0 || choice > 4) {
                    System.out.print("Choice: ");
                    choice = input.nextInt();
                }

                switch (choice) {
                    case 1:
                        viewAvailableCars(user, site);
                        break;
                    case 2:
                        viewRentedCars(user, site);
                        break;
                    case 3:
                        viewGivenCarsForRent(user, site);
                        break;
                    case 4:
                        addCarForRent(user, site);
                        break;
                    case 0:
                        return;
                }
            }
        } else {
            System.out.println('\n' + "Wrong username or password.");
        }
    }

    public static void registration(RentCarSite site) {
        Scanner input = new Scanner(System.in);
        String username, password;

        System.out.print('\n' + "Username: ");
        username = input.nextLine();

        System.out.print("Password: ");
        password = input.nextLine();

        User user = site.registration(username, password);

        if (user != null) {
            System.out.println('\n' + "Successful registration");
        } else {
            System.out.println('\n' + "Failed registration - this username already exist");
        }
        // registration successful ili this username is already used
    }

    public static void viewAvailableCars(User user, RentCarSite site) {
        Scanner input = new Scanner(System.in);
        Map<Integer, Car> cars = site.getCars();

        while (true) {
            System.out.println();
            cars.forEach((key, car) -> {
                if (car.isAvailable()) {
                    System.out.println(car.getId() + ". " + car.getModel() + " - " + car.getPrice() + '$');
                }
            });
            System.out.println("0. Back" + '\n');

            int choice = -1;
            Car chosenCar = null;

            do {
                System.out.print("Choice: ");
                choice = input.nextInt();
                chosenCar = cars.get(choice);
                if (choice == 0) {
                    break;
                }
            } while ((chosenCar == null) || !chosenCar.isAvailable());

            if (choice != 0) {
                System.out.println();
                chosenCar.view();

                System.out.println('\n' + "1. Rent this car");
                System.out.println("0. Back" + '\n');

                do { // ili remove
                    System.out.print("Choice: ");
                    choice = input.nextInt();
                } while (choice < 0 || choice > 1);
                input.nextLine();

                if (choice == 1) {
                    if (user != null) {
                        List<Car> userGivenCars = user.getGivenCars();
                        if(userGivenCars.contains(chosenCar)){
                            System.out.println('\n' + "Failed to rent this car - this car is yours :D");
                        }
                        else {
                            user.rentCar(chosenCar);
                            System.out.println('\n' + "You have successfully rented this car");
                        }
                    } else {
                        System.out.println('\n' + "You must be logged in to rent cars");
                    }
                    System.out.print('\n' + "Enter a key to go back: ");
                    input.nextLine();
                }
            } else {
                return;
            }
        }
    }

    public static void viewRentedCars(User user, RentCarSite site) {
        Scanner input = new Scanner(System.in);
        List<Car> rentedCars = user.getRentedCars();

        while (true) {
            System.out.println();
            int cnt = 1;
            for (Car car : rentedCars) {
                System.out.println(cnt + ". " + car.getModel() + " - " + car.getPrice() + '$');
                ++cnt;
            }

            System.out.println("0. Back" + '\n');
            int choice = -1;

            do {
                System.out.print("Choice: ");
                choice = input.nextInt();
            } while (choice < 0 || choice >= cnt);
            input.nextLine();

            if (choice != 0) {
                Car carChosen = rentedCars.get(choice - 1);
                System.out.println();
                carChosen.view();

                System.out.println('\n' + "1. Give back ");
                System.out.println("0. Go back");

                do {
                    System.out.print("Choice: ");
                    choice = input.nextInt();
                } while(choice < 0 || choice > 1);

                if(choice == 1){
                    user.giveBackCar(carChosen);
                }
            } else {
                return;
            }
        }
    }

    public static void viewGivenCarsForRent(User user, RentCarSite site) {
        Scanner input = new Scanner(System.in);
        List<Car> givenCars = user.getGivenCars();

        while (true) {
            System.out.println();
            int cnt = 1;
            for (Car car : givenCars) {
                System.out.println(cnt + ". "  + car.getModel() + " - " + car.getPrice() + '$');
                ++cnt;
            }

            System.out.println("0. Back" + '\n');
            int choice = -1;

            do {
                System.out.print("Choice: ");
                choice = input.nextInt();
            } while (choice < 0 || choice >= cnt);
            input.nextLine();

            if (choice != 0) {
                Car carChosen = givenCars.get(choice - 1);
                System.out.println();
                carChosen.view();
                System.out.print('\n' + "Enter a key to go back: ");
                input.nextLine();
            } else {
                return;
            }
        }
    }

    public static void addCarForRent(User user, RentCarSite site) {
        Scanner input = new Scanner(System.in);

        String model;
        System.out.print('\n' + "Model: ");
        model = input.nextLine();

        double price;
        System.out.print("Price: ");
        price = input.nextDouble();
        input.nextLine();

        site.addCar(user, model, price);
    }
}
