package entities;

import java.util.HashMap;
import java.util.Map;

public class RentCarSite {

    private int userIdGenerate = 1;
    private int carIdGenerate = 1;

    private Map<String, Account> accounts;
    private Map<Integer, User> users; // Map<String, User> ??? vmesto accounts...
    private Map<Integer, Car> cars;

    public RentCarSite() {
        accounts = new HashMap<>();
        users = new HashMap<>();
        cars = new HashMap<>();
    }

    public Map<String, Account> getAccounts() {
        return accounts;
    }

    public Map<Integer, Car> getCars(){
        return cars;
    }

    public Car addCar(User renter, String model, double price) { // addCar(User, Car) ili addCar(User, model, price) - otvunka da q suzdada i podam ili v samiq metod
        Car newCar = new Car(carIdGenerate, renter, model, price);
        renter.giveForRent(newCar);
        cars.put(carIdGenerate, newCar);
        ++carIdGenerate;
        return newCar;
    }

    public Map<Integer, User> getUsers() {
        return users;
    }

    public User login(String username, String password) {
        Account acc = accounts.get(username);
        if ((acc != null) && (acc.getPassword().equals(password))) {
            int id = acc.getId();
            return users.get(id);
        }
        return null;
    }

    public User registration(String username, String password){
        if (accounts.get(username) == null) {
            Account newAccount = new Account(userIdGenerate, username, password);
            User newUser = new User(userIdGenerate, username, password);
            accounts.put(username, newAccount);
            users.put(userIdGenerate, newUser);
            ++userIdGenerate;
            return newUser;
        }
        return null;
    }
}
