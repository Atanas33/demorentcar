package entities;

import java.util.ArrayList;
import java.util.List;

public class User {

    private final int id;
    private String username;
    private String password;
    //napravi gi Map ?? bazi - list?
    private List<Car> rentedCars; // id ili cqlata kola???
    private List<Car> givenCars; // id ili cqlata kola??? - red 32

    public User(int id, String username, String password) {
        this.id = id;
        this.username = username;
        this.setPassword(password);
        this.rentedCars = new ArrayList<>();
        this.givenCars = new ArrayList<>();
    }

    public int getId() {
        return id;
    }

    public String getUsername(){
        return username;
    }

    public String getPassword(){
        return password;
    }

    public void setPassword(String password){
        this.password = password;
    }

    public List<Car> getRentedCars(){
        return rentedCars;
    }

    public List<Car> getGivenCars(){
        return givenCars;
    }

    public void rentCar(Car car){
        if(!rentedCars.contains(car)) {
            rentedCars.add(car);
            car.setAvailable(false); // promenq se i v RentCarSite.Cars - is it ok?? - List<Cars> / List<IDs>
        }
    }

    public void giveBackCar(Car car){
        if(rentedCars.contains(car)) {
            rentedCars.remove(car);
            car.setAvailable(true);
        }
    }

    public void giveForRent(Car car){
        givenCars.add(car);
        car.setAvailable(true);
    }
}
